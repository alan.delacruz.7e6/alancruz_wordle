import java.util.*

fun main() {
    println("Bienvenido a Wordle!")
    println("Objectiu del joc: endevinar una paraula concreta de cinc lletres en un màxim de sis intents escrivint lletra per lletra.")
    val scanner = Scanner(System.`in`)
    println("Introdueix una lletra")
    val letra = scanner.next()
    val palabras = arrayListOf<String>("cabra", "amada", "mundo", "acero", "coche")
    val keyword = palabras.random()

    println(keyword)
}
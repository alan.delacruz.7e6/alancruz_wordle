import kolor.*
import java.lang.Exception
import java.util.*


//ALMACEN DE LAS PALABRAS
var respondidas = mutableListOf<String>()
fun main() {
    println("Bienvenido a Wordle!")
    println("Introduce una palabra de 5 letras para poder empezar: ")

    val scanner = Scanner(System.`in`)
    val palabras = arrayListOf("boyas", "alijo", "mundo", "acero", "acepto", "arcos", "agudo", "agrio", "jueza", "feliz", "veloz", "cajón", "marzo", "pinza", "zebra", "afine", "altos", "ambos", "aptos", "arpón", "acido")
    val keyword = palabras.random()
    var intents = 0


    while (intents != 6) {
        var palabra = ""
        var distancia = 0
        while (distancia != 5) {
            palabra = scanner.next().lowercase()
            distancia = palabra.length
            if (palabra.length != 5) println("Ha de ser amb 5 lletres")
        }

        //COMPROBACIÓN CARACTER POR CARACTER

        palabra.forEachIndexed { index, c ->
            if (c == keyword[index]) {
                respondidas.add(" $c".greenBackground())
            } else if (c in keyword) {
                respondidas.add(" $c".yellowBackground())
            } else if (c != keyword[index]) {
                respondidas.add(" $c".blackBackground())
            }
        }
        if (palabra == keyword) {
            registros()
            println("Has guanyat!")
            return
        }
        registros()
        intents++
    }
    println("Has perdut! La paraula era: ")
    print(keyword)

}

//MOSTRAR EL HISTORIAL GUARDADO

fun registros() {
    respondidas.forEachIndexed { index, valor ->
        if (index % 5 == 0) println()
        print(valor)
    }
    println()
}

